//
//  UIImageView+Ext.swift
//  CarViewer
//
//  Created by Brix Fernando on 25/03/2019.
//  Copyright © 2019 Brix Fernando. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {

  func imageFromUrl(url: String, placeholder: UIImage? = nil) {
    let compatibleUrl = url.replacingOccurrences(of: " ", with: "%20")
    self.sd_setImage(with: URL(string: compatibleUrl)!, placeholderImage: UIImage(named: "placeholder"), options: .scaleDownLargeImages, progress: nil) { (image, error, _, _) in
      if error != nil {
        print("[ERROR] \(String(describing: error))")
      } else {
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        self.layer.add(transition, forKey: nil)
        self.image = image
      }
    }
  }
}

