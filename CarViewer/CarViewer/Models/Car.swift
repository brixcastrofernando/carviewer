//
//  Car.swift
//  CarViewer
//
//  Created by Brix Fernando on 25/03/2019.
//  Copyright © 2019 Brix Fernando. All rights reserved.
//

import SwiftyJSON

struct Car {
  var image: [String]
  var name: String
  var price: String
  var brand: String

  var brandModelEdition: String
  var interior: String
  var exterior: String
  var equipment: String
  var description: String

  init() {
    self.image = []
    self.name = ""
    self.price = ""
    self.brand = ""

    self.brandModelEdition = ""
    self.interior = ""
    self.exterior = ""
    self.equipment = ""
    self.description = ""
  }

  init(carJSON: JSON) {
    let images = carJSON["images"]
    self.image = images.map { $0.1["url"].stringValue }

    let data = carJSON["data"]
    self.name = data["name"].stringValue
    self.price = data["price"].stringValue
    self.brand = data["brand"].stringValue

    self.brandModelEdition = data["brand_model_edition"].stringValue
    self.interior = data["interior"].stringValue
    self.exterior = data["exterior"].stringValue
    self.equipment = data["equipment"].stringValue
    self.description = data["description"].stringValue
  }
}
