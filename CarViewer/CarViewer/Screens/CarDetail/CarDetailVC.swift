//
//  CarDetailVC.swift
//  CarViewer
//
//  Created by Brix Fernando on 26/03/2019.
//  Copyright © 2019 Brix Fernando. All rights reserved.
//

import UIKit

class CarDetailVC: UIViewController {

  var car: Car!

  convenience init(car: Car) {
    self.init()
    self.car = car
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = UIColor.white
    self.title = car.name
    self.setupUI()
  }

  private func setupUI() {
    let textView = UITextView()
    self.view.addSubview(textView)

    textView.translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint(item: textView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 20).isActive = true
    NSLayoutConstraint(item: textView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -20).isActive = true
    NSLayoutConstraint(item: textView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 20).isActive = true
    NSLayoutConstraint(item: textView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: -20).isActive = true

    textView.text = """
    Brand Model Edition: \(self.car.brandModelEdition)
    Interior: \(self.car.interior)
    Exterior: \(self.car.exterior)
    Equipment: \(self.car.equipment)

    Description: \(self.car.description)
    """
  }
}
