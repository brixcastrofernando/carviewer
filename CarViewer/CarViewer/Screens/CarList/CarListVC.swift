//
//  CarListVC.swift
//  CarViewer
//
//  Created by Brix Fernando on 25/03/2019.
//  Copyright © 2019 Brix Fernando. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON

class CarListVC: UIViewController {

  private var carListView: CarListView!

  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    self.carListView.frame = self.view.bounds
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = UIColor.lightGray
    self.title = "Car List"
    self.setupUI()
  }

  private func setupUI() {
    if self.carListView == nil {
      self.carListView = CarListView()
      self.carListView.frame = self.view.bounds
      self.carListView.delegate = self
      self.carListView.currentPage = 1
      self.carListView.handleTapCell = { car in
        self.gotoCarDetail(car: car)
      }
      self.view.addSubview(self.carListView)
    }
  }

  private func gotoCarDetail(car: Car) {
    let carDetailVC = CarDetailVC(car: car)
    self.navigationController?.pushViewController(carDetailVC, animated: true)
  }
}

extension CarListVC: CarListViewDelegate {

  func loadNextPage(carListView: CarListView, currentPage: Int) {
    SVProgressHUD.show()
    AF.request("https://www.carmudi.co.id/api/cars/page:\(currentPage)/maxitems:10").responseJSON { response in
      SVProgressHUD.dismiss()
      carListView.isLoadingData = false
      if let _ = response.result.error {
        // prompt error message
        return
      }

      if let response: AnyObject = response.result.value as AnyObject? {
        let _responseJSON = JSON(response)
        guard _responseJSON["success"].boolValue == true else {
          // something went wrong
          return
        }

        let metadata = _responseJSON["metadata"]
        let results = metadata["results"]
        carListView.loadData(data: results.map { Car(carJSON: $0.1) })
      }
    }
  }
}
