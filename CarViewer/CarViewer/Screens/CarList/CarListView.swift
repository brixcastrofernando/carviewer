//
//  CarListView.swift
//  CarViewer
//
//  Created by Brix Fernando on 25/03/2019.
//  Copyright © 2019 Brix Fernando. All rights reserved.
//

import UIKit

protocol CarListViewDelegate: class {
  func loadNextPage(carListView: CarListView, currentPage: Int)
}

class CarListView: UIView {

  weak var delegate: CarListViewDelegate!

  private let cellId = "Cell"
  private let maxItems = 10

  var tableView: UITableView!
  var isLoadingData = false
  var currentPage: Int! {
    didSet {
      self.delegate.loadNextPage(carListView: self, currentPage: currentPage)
    }
  }
  var carArray: [Car] = [] {
    didSet {
      self.tableView.reloadData()
    }
  }

  var handleTapCell: ((Car)->())?

  override func didMoveToSuperview() {
    self.setupUI()
  }

  private func setupUI() {
    guard tableView == nil else {
      return
    }

    self.tableView = UITableView(frame: .zero, style: .plain)
    self.tableView.delegate = self
    self.tableView.dataSource = self
    self.addSubview(self.tableView)
    self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
    self.tableView.tableFooterView = UIView()

    self.tableView.translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint(item: self.tableView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0).isActive = true
    NSLayoutConstraint(item: self.tableView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
    NSLayoutConstraint(item: self.tableView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0).isActive = true
    NSLayoutConstraint(item: self.tableView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0).isActive = true

    let refresh = UIRefreshControl()
    refresh.addTarget(self, action: #selector(pulledToRefresh(_:)), for: .valueChanged)
    self.tableView.addSubview(refresh)
  }

  @objc private func pulledToRefresh(_ refreshControl: UIRefreshControl) {
    carArray = []
    currentPage = 1
    self.delegate.loadNextPage(carListView: self, currentPage: currentPage)
    refreshControl.endRefreshing()
  }
}

extension CarListView: UITableViewDelegate, UITableViewDataSource {

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return carArray.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

    let cell: UITableViewCell = UITableViewCell(style: .subtitle, reuseIdentifier: cellId)

    let car = carArray[indexPath.row]
    cell.textLabel?.text = "\(car.name)"
    cell.detailTextLabel?.text = "\(car.brand) - \(car.price)"

    if car.image.count > 0 {
      cell.imageView?.imageFromUrl(url: car.image.first!)
    }
    return cell
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 70.0
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    let car = carArray[indexPath.row]
    self.handleTapCell?(car)
  }

  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    guard isLoadingData == false else { return }
    guard indexPath.row >= carArray.count - (maxItems/2) else { return }

    isLoadingData = true
    self.currentPage += 1
  }

  func loadData(data: [Car]) {
    self.carArray.append(contentsOf: data)
    self.isLoadingData = false
  }
}
