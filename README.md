CarViewer App for iOS
===========================================

The CarViewer App is an application used to browse cars for sale. It has two pages, the landing page which contains the list of the cars, and the second page which contains the details about the car, which can be accessed by tapping on any of the item on the list on the first page.

The project is built using XCode 10.0 with iOS SDK 12 using Swift 4.2
The project uses Cocoapods as dependency manager and uses the following 3rd party libraries:
Alamofire - for doing network calls
SVProgressHUD - to show network activity and disable user interaction
SwiftyJSON - for simple parsing of network data
SDWebImage - for caching of images

The application runs only as an iPhone app but maybe viewed on iPad as an scaled up version app.
The application supports both portrait and landscape.
The application has the pull to refresh feature as well as the infinite scrolling.
Only the image are cached and no other data are stored on a local persistent store.
Ability to sort is not yet implemented and no unit test were done.
